/*
 * dicom-xnat-util: org.nrg.dcm.MatchedPatternExtractor
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm;

import java.util.SortedSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.util.TagUtils;
import org.nrg.framework.utilities.SortedSets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

/**
 * 
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 *
 */
public class MatchedPatternExtractor implements Extractor {
    private final Logger logger = LoggerFactory.getLogger(MatchedPatternExtractor.class);
    private final int tag, group;
    private final Pattern pattern;

    public MatchedPatternExtractor(final int tag, final Pattern pattern, final int group) {
        this.tag = tag;
        this.pattern = pattern;
        this.group = group;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.Extractor#extract(org.dcm4che2.data.DicomObject)
     */
    public String extract(final DicomObject o) {
        final String v = o.getString(tag);
        if (Strings.isNullOrEmpty(v)) {
            logger.trace("no match to {}: null or empty tag", this);
            return null;
        } else {
            final Matcher m = pattern.matcher(v);
            if (m.matches()) {
                logger.trace("input {} matched rule {}", v, this);
                return m.group(group);
            } else {
                logger.trace("input {} did not match rule {}", v, this);
                return null;
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.Extractor#getTags()
     */
    public SortedSet<Integer> getTags() {
        return SortedSets.singleton(tag);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString());
        sb.append(":").append(TagUtils.toString(tag)).append("~");
        sb.append(pattern).append("[").append(group).append("]");
        return sb.toString();
    }
}
